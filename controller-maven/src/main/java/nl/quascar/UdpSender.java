package nl.quascar;

import java.net.*;

public class UdpSender {
    private int port = 4210;
    private InetAddress address;

    // variables used by background thread
    private volatile int command = 0;
    private volatile boolean isRunning = false;

    public UdpSender() { }

    public void setClientIpAddress(String name){
        try{
            address = InetAddress.getByName(name);
        }catch (Exception ex){
            address = null;
            System.err.println("Provided invalid Ip address");
            ex.printStackTrace();
        }
    }

    public void setPort(int port){
        this.port = port;
    }

    @SuppressWarnings("unused")
    public String getClientAddress(){
        return address == null ? null : address.toString() + ":" + port;
    }

    public void updateCommand(int c){
        this.command = c;
    }

    public void stop(){
        isRunning = false;
    }

    public boolean getIsRunning(){
        return isRunning;
    }

    // a background task to communicate with the quascar over udp
    private class CommunicationTask implements Runnable {
        @Override
        public void run() {
            try{
                // udp socket
                DatagramSocket socket = new DatagramSocket();
                byte[] buf;
                while(isRunning){
                    buf = new byte[]{(byte) command};
                    DatagramPacket packet = new DatagramPacket(buf, 1, address, port);
                    socket.send(packet);

                    //trying to limit the number of udp packets sent over the network
                    Thread.sleep(50);
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    public void run() {
        if(address == null){
            System.err.println("IP address not initialized.");
            return;
        }

        isRunning = true;
        new Thread(new CommunicationTask()).start();
    }
}