package nl.quascar;

import net.java.games.input.Event;

public interface USBEventListener {
    void onEventReceived(Event event);
}
