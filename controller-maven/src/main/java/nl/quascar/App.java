package nl.quascar;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    public static final String broker = "tcp://192.168.137.1:1883";
    public static final String car1IP = "192.168.137.2";
    public static final String car2IP = "192.168.137.3";
    public static final int port = 4210;
    public static boolean USB_ENABLED = false;

    private static Scene scene;

    @Override
    public void start(Stage primaryStage) throws IOException {
        //We load the FXML file and set some stuff up to get a screen
        FXMLLoader loader = new FXMLLoader(getClass().getResource("startScreen.fxml"));
        Parent root = loader.load();
        StartScreen startScreen = loader.getController();
        primaryStage.setTitle("Quascar");
        primaryStage.setScene(new Scene(root));
        primaryStage.setOnCloseRequest(windowEvent -> System.exit(0));
        startScreen.initData(primaryStage);
        primaryStage.show();
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}