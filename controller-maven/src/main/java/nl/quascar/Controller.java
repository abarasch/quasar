package nl.quascar;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import net.java.games.input.Event;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.IOException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class Controller {
    public GridPane grid_pane;
    public Text server_status;
    public Text other_car_health;
    public Slider laserSlider;
    public CheckBox detect_usb_enabled;
    public ImageView powerupImage;
    public ImageView gradientImage;
    public ImageView healthBar;

    private ExtraController extraController;
    private boolean laserOverHeated = false;
    private Timer laserTimer;
    private UdpSender udpSender;
    private int command = 0;

    private Powerup savedPowerup;
    private int selectedCar;
    private Stage thisStage;

    MyMqttHandler mqtt;

    // used by the DEBUG button
    public void getPup(ActionEvent actionEvent) {
        Random random = new Random();
        onReceivePowerup(random.nextInt(8) + 1);
    }

    // a listener for the USB controller
    private class ExtraController extends USBControllerListener{
        @Override
        public void onEventReceived(Event event) {
            var action = Translate.usbEvent(event);
            controllerAction(action.getKey(), action.getValue());
        }
    }

    // Creates the Topic of a MQTT message based on the selected car, using the topic format of the Enumerator
    private String thisCarTopic(MqttHandler.Topic topic) {
        return String.format(topic.getTopic(),selectedCarIP());
    }

    // Same as thisCarTopic, but uses the IP of the other car.
    private String otherCarTopic(MqttHandler.Topic topic) {
        return String.format(topic.getTopic(), otherCarIP());
    }

    // Used for sending and receiving MQTT messages
    private class MyMqttHandler extends MqttHandler{
        public MyMqttHandler(){
            super(App.broker);
        }

        public MyMqttHandler(MqttClient mqttClient) {
            super(mqttClient, App.broker);
        }

        // creates an int type from a payload that is a number as a string
        private int asInt(MqttMessage mqttMessage) {
            return Integer.parseInt(new String(mqttMessage.getPayload()));
        }

        @Override
        public void connectionLost(Throwable throwable) {
            System.out.println("Lost connection to mqtt broker");
        }

        // Called when a MQTT message arrives that we are subsribed to.
        @Override
        public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
            // what happens when a message arrives
            System.out.println(s + ", " + mqttMessage.toString());

            // avoid Not on FX application thread Exception:
            Platform.runLater(() -> {
                if(s.equals(thisCarTopic(Topic.HEALTH))) {
                    onHealthUpdateThisCar(asInt(mqttMessage));
                } else if(s.equals(String.format(Topic.HEALTH.getTopic(), otherCarIP()))) {
                    onHealthUpdateOtherCar(asInt(mqttMessage));
                } else if(s.equals(thisCarTopic(Topic.GAME_STATE))) {
                    String message = new String(mqttMessage.getPayload());
                    if(message.equals(selectedCarIP())) endGame(true);
                    if(message.equals(otherCarIP())) endGame(false);
                } else if(s.equals(thisCarTopic(Topic.GET_POWERUP))) {
                    onReceivePowerup(Powerup.valueOf(new String(mqttMessage.getPayload())).number);
                }
            });
        }

        @Override
        protected void connected() {
            server_status.setText("connected");
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
            System.out.println("Mqtt delivery complete");
        }
    }

    private boolean laserPressed = false;
    //handles laser overheat functionality..
    private class LaserTask extends TimerTask {
        Image image;
        @Override
        public void run() {
            if(laserPressed){
                laserSlider.setValue(laserSlider.getValue() + 7);
                if(laserSlider.getValue() >= laserSlider.getMax()){ // laser overheated
                    controllerAction(Translate.SHOOT, false); //force a key up on laser
                    image = new Image(getClass().getResourceAsStream("/nl/quascar/cooling_down.png"),200,20,false,false);
                    gradientImage.setImage(image);
                    laserOverHeated = true;
                    laserPressed = false;
                }
            }
            else{
                laserSlider.setValue(laserSlider.getValue() - 2);
                if(laserSlider.getValue() <= laserSlider.getMin()) {//laser cools down
                    image = new Image(getClass().getResourceAsStream("/nl/quascar/gradient.png"),200,20,false,false);
                    gradientImage.setImage(image);
                    laserOverHeated = false;
                }
            }
        }
    }

    // Needs to be called when creating an instance of this class. The stage is the one that the previous stage has,
    // so we can change this stage into the next one. selectedCar should be 1 or 2. MqttClient can be passed so we don't
    // need to reestablish a connection.
    public void initData(int selectedCar, Stage stage, MqttClient mqttClient) {
        this.selectedCar = selectedCar;
        thisStage = stage;
        mqtt = new MyMqttHandler(mqttClient);
        mqtt.setCallBack(mqtt);
    }

    //is called after the scene is constructed
    public void initialize(){
        try{
            //default values:
            detect_usb_enabled.setSelected(App.USB_ENABLED);

            //keyboard event handlers
            grid_pane.addEventHandler(KeyEvent.KEY_PRESSED,
                    (key) -> controllerAction(Translate.keycode(key.getCode()), true));

            grid_pane.addEventHandler(KeyEvent.KEY_RELEASED,
                    (key) -> controllerAction(Translate.keycode(key.getCode()), false));

            extraController = new ExtraController();

            //start timer that handles laser overheated
            laserTimer = new Timer();
            laserTimer.schedule(new LaserTask(), 0, 5000 );

            //start listening to usb controller input
            if(detect_usb_enabled.isSelected())
                extraController.start();

            //server object will send udp packets with a byte representing commands to quascar.
            udpSender = new UdpSender();
            udpSender.setClientIpAddress(selectedCarIP());
            udpSender.setPort(App.port);
            udpSender.run();

            //update interface
            if(udpSender.getIsRunning())
                server_status.setText("Sending command: " + command);

        } catch (Exception ex){
            System.err.println("Error while initializing view.");
            ex.printStackTrace();
        }
    }

    // Returns the IP of the selected car as a String
    private String selectedCarIP() {
        return (selectedCar == 1) ? App.car1IP : App.car2IP;
    }

    // Returns the IP of the other car as a String
    private String otherCarIP() {
        return (selectedCar == 2) ? App.car1IP : App.car2IP;
    }

    // Called when a key is pressed or unpressed. If isKeyDown is true it's a key press, and an unpress otherwise.
    // The action should be one of the ones in the Translate class.
    private void controllerAction(int action, boolean isKeyDown){
        if(udpSender == null || !udpSender.getIsRunning())
            return;

        if(action == Translate.SHOOT){
            if(laserOverHeated)
                return;
            laserPressed = isKeyDown;
        }
        if(action == Translate.POWERUP && isKeyDown){
            activatePowerup();
        } else {
            if (isKeyDown)
                addCommand(action);
            else
                removeCommand(action);
        }
    }

    // Sends the saved powerup over MQTT to the Wemos and sets the current slot as empty.
    private void activatePowerup() {
        if(savedPowerup != null){
            String topic = savedPowerup.positive ?
                    thisCarTopic(MqttHandler.Topic.DO_POWERUP) :
                    otherCarTopic(MqttHandler.Topic.DO_POWERUP);

            mqtt.publish(topic, Integer.toString(savedPowerup.number));

            Image image = new Image(getClass().getResourceAsStream("/nl/quascar/powerups/empty.png"));
            powerupImage.setImage(image);
            savedPowerup = null;
        }
    }

    // The command is a single char and has an encoding for each different bit. command is send as often as possible
    // commandToAdd should be one of the commands from the Translate class (but not POWERUP)
    private void addCommand(int commandToAdd){
        command = command | commandToAdd;
        updateCommand();
    }

    // Removes the input from the command that is sent.
    private void removeCommand(int commandToRemove){
        command = command & ~commandToRemove;
        updateCommand();
    }

    // actually updates the command that is sent in the UDP sender (and also the feedback).
    private void updateCommand(){
        udpSender.updateCommand(command);
        if(udpSender.getIsRunning())
            server_status.setText("Sending command: " + command);
    }

    // Called when a health update for the selected car is received over MQTT. Sets the width of the health bar based on the health received.
    private void onHealthUpdateThisCar(int newHealth) {
        Image image = new Image(getClass().getResourceAsStream("/nl/quascar/health_bar.png"), 2 * newHealth, 20, false, false);
        healthBar.setImage(image);
    }

    // Called when a health update for the other car is received over MQTT. Sets the health as a text
    private void onHealthUpdateOtherCar(int newHealth) {
        other_car_health.setText(String.valueOf(newHealth));
    }

    private void onReceivePowerup(int powerupNumber) {
        savedPowerup = Powerup.getByNumber(powerupNumber);
        Image image = new Image(getClass().getResourceAsStream("/nl/quascar/powerups/" + savedPowerup.imageFile));
        powerupImage.setImage(image);
    }

    // used by a DEBUG button to manually test the ending of a game
    public void winGame(ActionEvent mouseEvent) {
        endGame(true);
    }

    // used by a DEBUG button to manually test the ending of a game
    public void loseGame(ActionEvent mouseEvent) {
        endGame(false);
    }

    // called when the game ends. Opens the next screen. winner should be set to true if the selected car is the winner.
    private void endGame(boolean winner) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("endScreen.fxml"));
            Parent root = fxmlLoader.load();

            EndScreen controller = fxmlLoader.getController();
            controller.initData(winner, thisStage);

            thisStage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}