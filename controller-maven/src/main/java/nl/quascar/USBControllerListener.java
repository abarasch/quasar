package nl.quascar;

import net.java.games.input.*;
import net.java.games.input.Controller;

import java.util.Arrays;

// We have only programmed our software to work with one specific controller: The Big Ben PS3 Controller
public abstract class USBControllerListener implements USBEventListener {
    private volatile boolean isRunning = false;

    public void stop(){
        isRunning = false;
    }

    // a background task that listens to usb controller input
    private class CommunicationTask implements Runnable{
        @Override
        public void run() {
            //following main example of input.
            Controller[] controllers = ControllerEnvironment
                    .getDefaultEnvironment().getControllers();

            if (controllers.length == 0) {
                System.out.println("Found no controllers.");
                isRunning = false;
                return;
            }

            while (isRunning) {
                for (Object obj : Arrays.stream(controllers).filter(x -> x.getType().toString().equals("Stick")).toArray()) {
                    Controller controller = (Controller)obj;
                    controller.poll();

                    if(!controller.getType().toString().equals("Stick"))
                        continue;

                    EventQueue queue = controller.getEventQueue();
                    Event event = new Event();
                    while (queue.getNextEvent(event)) {
                        onEventReceived(event);
                    }
                }

                //analog input sends updates too often, trying to limit that input
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void start() {
        isRunning = true;
        new Thread(new CommunicationTask()).start();
    }
}
