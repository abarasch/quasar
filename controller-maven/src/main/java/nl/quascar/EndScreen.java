package nl.quascar;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;

public class EndScreen {
    public ImageView EndScreenImage;
    private Stage thisStage;

    // This function should be called when creating an instance of this class. It sets the image that tells the user
    // who won and the stage can be used so we don't need to create a new window.
    public void initData(boolean winner, Stage stage) {
        thisStage = stage;
        Image image = new Image(getClass().getResourceAsStream(winner ? "/nl/quascar/WIN.png" : "/nl/quascar/LOSS.png"));
        EndScreenImage.setImage(image);
    }

    // called by the restart button. Sets up and changes stage to the start screen.
    public void restartEvent(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("startScreen.fxml"));
            Parent root = fxmlLoader.load();

            StartScreen controller = fxmlLoader.getController();
            controller.initData(thisStage);

            thisStage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
