package nl.quascar;

public enum Powerup {
    SPEEDUP(1, true, "SPEEDUP.png"),
    LOCK_INPUT(2, false, "LOCK_INPUT.png"),
    SPIN(3, false, "SPIN.png"),
    CONTINUOUS_LASER(4, true, "CONTINUOUS_LASER.png"),
    SWITCH_DIRECTIONS(5, false, "SWITCH_DIRECTIONS.png"),
    CANT_STOP(6, false, "CANT_STOP.png"),
    CANT_REVERSE(7, false, "CANT_REVERSE.png"),
    CANT_FORWARD(8, false, "CANT_FORWARD.png"),
    HEALTH_UP(9, true, "HEALTH_UP.png");

    // The number that corresponds to this powerup
    int number;
    // whether or not the powerup is good or bad, used to determine to which car the powerup should be sent.
    boolean positive;
    // the image associated with the powerup
    String imageFile;

    Powerup(int number, boolean positive, String imageFile) {
        this.number = number;
        this.positive = positive;
        this.imageFile = imageFile;
    }

    static Powerup getByNumber(int powerupNumber) {
        return Powerup.values()[powerupNumber - 1];
    }

    @Override
    public String toString() {
        return number + ", " + positive;
    }
}
