package nl.quascar;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.ThreadLocalRandom;

public abstract class MqttHandler implements MqttCallback {

    protected abstract void connected();

    // The topics that we are able to receive over MQTT. All of them start with an IP of one of the cars, so to actually
    // use it, use StringFormat and add the IP.
    public enum Topic{
        HEALTH("health"),
        GAME_STATE("gameState"),
        GET_POWERUP("getPowerup"),
        DO_POWERUP("doPowerup"),
        READY_STATE("readyState");

        private String topic;
        Topic(String topicString){
            this.topic = "%s/" + topicString;
        }
        public String getTopic(){
            return topic;
        }
    }

    private MqttClient mqttClient;
    private String broker;
    private MemoryPersistence persistence = new MemoryPersistence();

    public MqttHandler(String _broker){
        broker = _broker;
    }

    public MqttHandler(MqttClient mqttClient, String broker) {
        this.mqttClient = mqttClient;
        this.broker = broker;
    }

    public MqttClient getMqttClient() {
        return mqttClient;
    }

    public void initialize() throws Exception{
        // We create a random name because the controllers cant have the same name.
        // This does mean there is a 1/1000 chance for an error each time you try to run it.
        String clientId = "JavaController-" + ThreadLocalRandom.current().nextInt(0, 1000);

        mqttClient = new MqttClient(broker, clientId, persistence);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        System.out.println("Connecting to broker: " + broker);
        mqttClient.connect(connOpts);
        connected();
        System.out.println("Connected");
    }

    // called to subscribe to a topic. This means if a message with that topic is sent to the broker we will receive it.
    private void subscribe(String topic){
        try{
            mqttClient.subscribe(topic);
        }catch (Exception ex){
            ex.printStackTrace();
            System.out.println("Failed to subscribe to topic: " + topic);
        }
    }

    // These are all the topics we want the controller to be subscribed to.
    public void subscribeTopics(String player, String opponent){
        subscribe(String.format(Topic.GET_POWERUP.getTopic(), player));
        subscribe(String.format(Topic.GAME_STATE.getTopic(), player));
        subscribe(String.format(Topic.HEALTH.getTopic(), player));
        subscribe(String.format(Topic.HEALTH.getTopic(), opponent));
    }

    // When you change the car you control you need to switch between topics. This function is used to unsubscribe to all
    // current subscriptions.
    public void clearSubscriptions(){
        try {
            for (Topic t : Topic.values()){
                mqttClient.unsubscribe(String.format(t.getTopic(), App.car1IP));
                mqttClient.unsubscribe(String.format(t.getTopic(), App.car2IP));
            }
        }catch (Exception ex){
            ex.printStackTrace();
            System.err.println("Failed to unsubscribe.");
        }
    }

    public void setCallBack(MqttCallback callBack){
        mqttClient.setCallback(callBack);
    }

    // Used to send a message to the MQTT broker.
    public void publish(String topic, String message){
        try{
            MqttMessage msg = new MqttMessage();
            msg.setPayload(message.getBytes(StandardCharsets.UTF_8));
            mqttClient.publish(topic, msg);
            System.out.println(String.format("Sent Topic: %s, Message %s", topic, message));
        }catch (Exception ex){
            ex.printStackTrace();
            System.err.println(String.format("Error sending message. Topic: %s, Message: %s", topic, message));
        }
    }
}
