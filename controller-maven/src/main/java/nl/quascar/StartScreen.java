package nl.quascar;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.IOException;


public class StartScreen {
    public ToggleButton car_toggle1;
    public ToggleButton car_toggle2;
    public ToggleButton ready_button;
    public ToggleGroup toggleGroupCar;
    public Text server_status;
    public Text waiting_message;
    public CheckBox detect_usb_enabled;
    public Button startButton;

    private MyMqttHandler mqtt;

    private boolean ready;
    private Stage thisStage;

    // Returns the IP of the selected car as a String
    private String selectedCarIP() {
        return car_toggle1.isSelected() ? App.car1IP : App.car2IP;
    }

    // Returns the IP of the other car as a String
    private String otherCarIP() {
        return car_toggle1.isSelected() ? App.car2IP : App.car1IP;
    }

    // The ready button (en/dis)ables the toggle buttons that select a car. It also sends an (un)ready message to
    // the MQTT broker.
    public void onReadyClicked(ActionEvent mouseEvent) {
        if(ready) {
            car_toggle1.setDisable(false);
            car_toggle2.setDisable(false);
            startButton.setDisable(true);
            waiting_message.setText("");
            ready_button.setText("I'm ready");
            mqtt.publish(String.format(MqttHandler.Topic.READY_STATE.getTopic(), selectedCarIP()), "0");
            mqtt.clearSubscriptions();
        } else {
            //sendReadyMessage(selectedCar());
            startButton.setDisable(false);
            car_toggle1.setDisable(true);
            car_toggle2.setDisable(true);
            waiting_message.setText("Waiting for other player...");
            ready_button.setText("Unready");
            mqtt.subscribeTopics(selectedCarIP(), otherCarIP());
            mqtt.publish(String.format(MqttHandler.Topic.READY_STATE.getTopic(), selectedCarIP()), "1");
        }
        ready = !ready;
    }

    // used by a DEBUG button to manually start the game without a signal from the server.
    public void onStartClicked(MouseEvent mouseEvent) {
        if(ready) startGame(selectedCar());
    }

    // has to be called when creating an instance of this class so that we don't need to create a new window.
    public void initData(Stage stage) {
        thisStage = stage;
    }

    // is always called by javafx to create the class.
    public void initialize(){
        try{
            //default values:
            ready = false;
            car_toggle1.setSelected(true);

            //usb controller handler
            System.out.println("USB State = "+ detect_usb_enabled.isSelected());

           // mqtt
            mqtt = new MyMqttHandler();
            mqtt.initialize();
            mqtt.setCallBack(mqtt);
        } catch (Exception ex){
            System.err.println("Error while initializing view.");
            ex.printStackTrace();
        }
    }

    // used for sending and receiving MQTT messages.
    private class MyMqttHandler extends MqttHandler{
        public MyMqttHandler(){
            super(App.broker);
        }

        @Override
        public void connectionLost(Throwable throwable) {
            System.out.println("Lost connection to mqtt broker");
        }

        @Override
        public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
            System.out.println(String.format("Received! Topic: %s, Message: %s.", s, mqttMessage.toString()));
            // what happens when a message arrives
            if(s.equals(String.format(Topic.GAME_STATE.getTopic(), selectedCarIP()))){
                if(Integer.parseInt(new String(mqttMessage.getPayload())) == 0) {
                    // avoid Not on FX application thread Exception:
                    Platform.runLater(() -> startGame(selectedCar()));
                }
            }
        }

        @Override
        protected void connected() {
            server_status.setText("connected");
        }

        @Override
        public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
            System.out.println("Mqtt delivery complete");
        }
    }

    // Called when an MQTT message is received that the game has started because everyone is ready.
    public void startGame(int selectedCar) {
        try {
            App.USB_ENABLED = detect_usb_enabled.isSelected();

            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("mainView.fxml"));
            Parent root = fxmlLoader.load();

            Controller controller = fxmlLoader.getController();
            controller.initData(selectedCar, thisStage, mqtt.getMqttClient());
            controller.initialize();

            thisStage.setScene(new Scene(root));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    // used to pass which car is selected to the next screen.
    private int selectedCar() {
        return car_toggle1.isSelected() ? 1 : 2;
    }

}
