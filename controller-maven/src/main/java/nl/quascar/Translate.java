package nl.quascar;

import javafx.scene.input.KeyCode;
import javafx.util.Pair;
import net.java.games.input.Event;

// This class is used to translate controller or keyboard input into a command.
public class Translate {
    public static final int NONE        = 0;
    public static final int FORWARD     = 1;
    public static final int BACKWARD    = 2;
    public static final int RIGHT       = 4;
    public static final int LEFT        = 8;
    public static final int LASER_RIGHT = 16;
    public static final int LASER_LEFT  = 32;
    public static final int SHOOT       = 64;
    public static final int POWERUP     = -1;


    public static int keycode(KeyCode code){
        int command = NONE;
        switch (code){
            case W: command = FORWARD; break;
            case S: command = BACKWARD; break;
            case D: command = RIGHT; break;
            case A: command = LEFT; break;
            case E: command = LASER_RIGHT; break;
            case Q: command = LASER_LEFT; break;
            case K: command = SHOOT; break;
            case J: command = POWERUP; break;
        }
        return command;
    }

    public static Pair<Integer, Boolean> usbEvent(Event event){
        // TODO
        String identifier = event.getComponent().getIdentifier().getName();
        float value = event.getValue();

        //TODO: check which controller it is:
        // I have two versions of the controllers.

        switch (identifier){
            /* problem with pov component is that it does not update on key-up
             so you need to remember the last command and when receiving a new one remove the old.
            */
            // Directional buttons pad:
            case "pov":
                if(value >= 1f)
                    return new Pair(LEFT, true);
                else if(value >= 0.875)
                    return new Pair(LEFT | BACKWARD, true);
                else if(value >= 0.75)
                    return new Pair(BACKWARD, true);
                else if(value >= 0.625)
                    return new Pair(BACKWARD | RIGHT, true);
                else if(value >= 0.5)
                    return new Pair(RIGHT, true);
                else if(value >= 0.375)
                    return new Pair(RIGHT | FORWARD, true);
                else if(value >= 0.25)
                    return new Pair(FORWARD, true);
                else if(value >= 0.125)
                    return new Pair(FORWARD | LEFT, true);
                else
                    return new Pair(FORWARD | BACKWARD | LEFT | RIGHT, false);

            // R1
            case "5":
                return new Pair(SHOOT, value == 1.0f);

            // L1
            case "4":
                return new Pair(POWERUP, value == 1.0f);

            //horizontal axis on right analog
            case "z":
                if(value > 0.5)
                    return new Pair(LASER_RIGHT, true);
                else if(value < -0.5)
                    return new Pair(LASER_LEFT, true);
                else
                    return new Pair(LASER_RIGHT | LASER_LEFT, false);

            // vertical axis on left analog
            case "y":
                if(value > 0.5)
                    return new Pair(BACKWARD, true);
                else if(value < -0.5)
                    return new Pair(FORWARD, true);
                else
                    return new Pair(BACKWARD | FORWARD, false);

            // vertical axis on left analog
            case "x":
                if(value > 0.5)
                    return new Pair(RIGHT, true);
                else if(value < -0.5)
                    return new Pair(LEFT, true);
                else
                    return new Pair(RIGHT | LEFT, false);

            default:
                return (Pair<Integer, Boolean>) new Pair(NONE, false);
        }
    }
}
