module nl.quascar {
    requires javafx.controls;
    requires javafx.fxml;
    requires jinput;
    requires org.eclipse.paho.client.mqttv3;

    opens nl.quascar to javafx.fxml;
    exports nl.quascar;
}