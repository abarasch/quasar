// ======= Server -> Controller =======
void broadcast(String topic, String message){
  for(Car c: game.getCars()){
    client.publish(c.getIP()+"/"+topic,message);
  }
}

void healthMessage(Car car){
  client.publish(car.getIP()+"/health",String.valueOf(car.getHealth()));
}

void getPowerup(Car car, PowerUp powUp){
  println("Sending get powerup");
  client.publish(car.getIP()+"/getPowerup",String.valueOf(powUp.getId()));
}

// ======= Controller -> Server =======
void readyState(String carIP, String message){
  if(message.equals("0")) getCarByIp(carIP).setReady(false);
  else getCarByIp(carIP).setReady(true);
}

// ======= Car -> Server =======
void rfid(String carIP, String message){
  Car c = getCarByIp(carIP);
  PowerUp p = PowerUp.getFromId(Integer.valueOf(message));
  if(p == PowerUp.HEALTH_UP){
    c.healthUp(10);
    healthMessage(c);  
  }else {
    c.setPowerUp(p); //Activate powerup for car with "carIP"
    getPowerup(c, p);
  }

  
}

void hit(String carIP, String message){
  Car c =getCarByIp(carIP);
  if(Integer.valueOf(message) == 1) c.hit(20); //1 = back / 2 = sides
  else c.hit(10);
  healthMessage(c); //Send the updated health back to controller;
}


// =======Helper functions =======

// Get opponent's Car object from IP
Car getOpponentByIp(String carIP){
  for(Car c:game.getCars()){
    if(!c.getIP().equals(carIP)) return c;
  }
  System.out.println("ERROR: Unable to find opponent's car with IP "+carIP);
  return new Car(-1,"-1");
}

// Get Car object from IP
Car getCarByIp(String carIP){
  for(Car c:game.getCars()){
    if(c.getIP().equals(carIP)) return c;
  }
  System.out.println("ERROR: Unable to find car with IP "+carIP);
  return new Car(-1,"-1");
}
