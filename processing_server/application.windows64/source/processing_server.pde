import mqtt.*;
import controlP5.*;

final String DEFAULT_URI = "tcp://127.0.0.1:1883";
final String CLIENT_ID = "QuascarGameServer" ;
final int MARGIN = 70;
boolean serverRunning = false;
QuascarGame game;
PFont font;
ControlP5 cp5;

String ipTextField = "";

MQTTClient client;

void setup() {
  size(800, 800);
  initColors();
  setFont();
  textAlign(CENTER);
  initControlP5();
  game = new QuascarGame();
}

void draw() {
  background(0);
  game.display();
}

// Initialize the "cp5" ControlP5 variable to intialize multiple UI
// related elements (buttons, text fields, text labels, etc.)
void initControlP5() {
  cp5 = new ControlP5(this);
  cp5.addLabel("gameStatus")
    .setText("Waiting for server to start running...")
    .setPosition(width/4, height/2+150)
    .setFont(font);
  cp5.addButton("toggleServer")
    .setCaptionLabel("Run Server")
    .setPosition(width/2+40, height-70)
    .setSize(180, 30)
    .setFont(font);
  cp5.addButton("defaultButton")
    .setCaptionLabel("Default")
    .setPosition(width/2+240, height-70)
    .setSize(100, 30)
    .setFont(font);
  cp5.addTextfield("URItextField")
    .setValue(DEFAULT_URI)
    .setCaptionLabel("URI")
    .setPosition(20, height-70)
    .setSize(400, 30)
    .setFont(font)
    .setFocus(true)
    .setColor(WHITE)
    ;
}

// Function to be executed when the "Default" button is pressed
// Sets the URI text field to a default URI
public void defaultButton() {
  cp5.get(Textfield.class, "URItextField").setValue(DEFAULT_URI);
}

// Function to be activated when the "Run Server / Stop server" button is pressed
public void toggleServer() {
  String URI = cp5.get(Textfield.class, "URItextField").getText();
  serverRunning = !serverRunning;
  if (serverRunning) { 
    String gameStatus = "";
    int t = initClient(URI);
    println(t);
    if (t==-1) {
      gameStatus = "ERROR: cannot run server with given URI";
      serverRunning = false;  
      cp5.get(Textfield.class, "URItextField").setColor(RED);
    } else {
      gameStatus = "Server running. Waiting for players to ready up...";
      cp5.get(Button.class, "toggleServer").setCaptionLabel("Stop server");
      cp5.get(Textfield.class, "URItextField").setColor(GREEN);
    }
    cp5.get(Textlabel.class, "gameStatus")
      .setText(gameStatus);
  } else {
    client.disconnect();
    cp5.get(Textfield.class, "URItextField").setColor(WHITE);
    cp5.get(Textlabel.class, "gameStatus")
      .setText("Waiting for server to start running...");
    cp5.get(Button.class, "toggleServer").setCaptionLabel("Run server");
  }
}

void setFont() {
  font = createFont("RussoOne-Regular.ttf", 20);
  textFont(font);
  textAlign(CENTER, CENTER);
}

PApplet getApplet() {
  return this;
}

// Initialize mqtt client with specified URI
// Return -1 if error has occurred
// Otherwise, return 1.
int initClient(String URI) {

  if (client == null) client = new MQTTClient(getApplet());

  try {
    client.connect(URI, CLIENT_ID);
    client.subscribe("#"); // Connect to all topics in this URI
  }
  catch(RuntimeException ex) {
    println("ERROR: failed to connect client using given information:\n|\tURI: "+URI+"\n|\tCLIENT_ID: "+CLIENT_ID);
    return -1;
  }
  return 1;
}

void clientConnected() {
  ArrayList<Car> cars = game.getCars();
  if (cars.size()>=2) return;

  println("New client connected.");
  cars.add(new Car(cars.size()+1, ""));
}

// New mqtt message received with given topic and payload
void messageReceived(String topic, byte[] payload) {
  String message = new String(payload);
  println("New message received: " + topic + " - " + message);
  String[] arr = topic.split("/");
  if (arr.length<2) return;
  if(arr[1].equals("readyState") && !game.gameRunning) readyState(arr[0], message);
  if(!game.gameRunning) return; // Don't run any "hit" or "rfid" commands if the game has not started yet
  
  switch(arr[1]) {
    case "rfid": 
      rfid(arr[0], message);
      break;
    case "hit": 
      hit(arr[0], message);
      break;
  }
}

void connectionLost() {
  println("ERROR: Client connection lost");
  exit();
}
