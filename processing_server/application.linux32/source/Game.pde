class QuascarGame{
  ArrayList<Car> cars;
  boolean gameRunning = false;
  
  QuascarGame(){
    cars = new ArrayList();
    cars.add(new Car(cars.size()+1,"192.168.137.2"));
    cars.add(new Car(cars.size()+1,"192.168.137.3"));
  }
  
  void display(){
    displayTitle();
    displayCars();
  }
  
  void restartCars(){
    for(Car c: cars) c.restartCar();
  }

  void startGame(){
    restartCars(); // Sets both cars to 100 health and no powerup
    gameRunning = true;
    cp5.get(Textlabel.class,"gameStatus").setText("Game started!");
    broadcast("gameState", "0"); // Send command to controllers that game has started
  }
  
  ArrayList<Car> getCars(){
    return cars;
  }
  
  void checkForGameStart(){
    for(Car c:cars){
      if(!c.getReadyStatus()) return;
    }
    startGame();
  }
  
  void displayTitle(){
    push();
    textSize(50);
    text("QUASCAR", width/2, height*0.1);
    textSize(30);
    text("CONNECTED CARS:", width*0.25, height*0.2);
    pop();
  }

  void gameOver(){
    for(Car c: cars){
      c.setReady(false);
    }
    gameRunning = false;
    Car winner = cars.get(0).getHealth()<cars.get(1).getHealth()?cars.get(1):cars.get(0);
    String mess = "GAME OVER: Player "+winner.getId()+" won!\nBoth players need to ready up to start a new game.";
    println(mess);
    cp5.get(Textlabel.class,"gameStatus").setText(mess);
    broadcast("gameState", winner.getIP()); // Send command to controllers that game has ended. Announce winner
  }

  void displayCars(){
    push();
    translate(50,height*0.25);
    for(Car car:cars){
      car.displayUI();
      translate(0,uiBoxSize.y+MARGIN);
    }
    pop();
  }
}
