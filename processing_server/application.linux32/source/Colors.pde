public color RED,GREEN,GREY,BLACK,WHITE,GREY_STROKE,FONT_COLOR;

void initColors(){
  colorMode(HSB,360,100,100);
  RED = color(0,100,100);
  GREEN = color(120,100,100);
  GREY = color(0,0,60);
  BLACK = color(0,0,0);
  WHITE = color(0,0,100);
  
  GREY_STROKE = color(0,0,50);
  FONT_COLOR = color(0,0,50);
}
