enum PowerUp{
  SPEEDUP, LOCK_INPUT, SPIN, CONTINUOUS_LASER, SWITCH_DIRECTION, CANT_STOP, CANT_REVERSE, CANT_FORWARD, HEALTH_UP;
  
  
  static PowerUp getFromId(int powId){
    switch(powId){
      case 1: return SPEEDUP;
      case 2: return LOCK_INPUT;
      case 3: return SPIN;
      case 4: return CONTINUOUS_LASER;
      case 5: return SWITCH_DIRECTION;
      case 6: return CANT_STOP;
      case 7: return CANT_REVERSE;
      case 8: return CANT_FORWARD;
      case 9: return HEALTH_UP;
      default: return SPEEDUP;
    }
  }
  
  int getId(){
    switch(this){
      case SPEEDUP: return 1;
      case LOCK_INPUT: return 2;
      case SPIN: return 3;
      case CONTINUOUS_LASER: return 4;
      case SWITCH_DIRECTION: return 5;
      case CANT_STOP: return 6;
      case CANT_REVERSE: return 7;
      case CANT_FORWARD: return 8;
      case HEALTH_UP: return 9;
       default: return 0;
    
    }
  
  }
  
  String toString(){
    switch(this){
      case SPEEDUP: return "SPEEDUP";
      case LOCK_INPUT: return "LOCK_INPUT";
      case SPIN: return "SPIN";
      case CONTINUOUS_LASER: return "CONTINUOUS_LASER";
      case SWITCH_DIRECTION: return "SWITCH_DIRECTION";
      case CANT_STOP: return "CANT_STOP";
      case CANT_REVERSE: return "CANT_REVERSE";
      case CANT_FORWARD: return "CANT_FORWARD";
      default: return "SPEEDUP";
    }
  }
}
