import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import mqtt.*; 
import controlP5.*; 
import java.util.Optional; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class processing_server extends PApplet {




final String DEFAULT_URI = "tcp://127.0.0.1:1883";
final String CLIENT_ID = "QuascarGameServer" ;
final int MARGIN = 70;
boolean serverRunning = false;
QuascarGame game;
PFont font;
ControlP5 cp5;

String ipTextField = "";

MQTTClient client;

public void setup() {
  
  initColors();
  setFont();
  textAlign(CENTER);
  initControlP5();
  game = new QuascarGame();
}

public void draw() {
  background(0);
  game.display();
}

// Initialize the "cp5" ControlP5 variable to intialize multiple UI
// related elements (buttons, text fields, text labels, etc.)
public void initControlP5() {
  cp5 = new ControlP5(this);
  cp5.addLabel("gameStatus")
    .setText("Waiting for server to start running...")
    .setPosition(width/4, height/2+150)
    .setFont(font);
  cp5.addButton("toggleServer")
    .setCaptionLabel("Run Server")
    .setPosition(width/2+40, height-70)
    .setSize(180, 30)
    .setFont(font);
  cp5.addButton("defaultButton")
    .setCaptionLabel("Default")
    .setPosition(width/2+240, height-70)
    .setSize(100, 30)
    .setFont(font);
  cp5.addTextfield("URItextField")
    .setValue(DEFAULT_URI)
    .setCaptionLabel("URI")
    .setPosition(20, height-70)
    .setSize(400, 30)
    .setFont(font)
    .setFocus(true)
    .setColor(WHITE)
    ;
}

// Function to be executed when the "Default" button is pressed
// Sets the URI text field to a default URI
public void defaultButton() {
  cp5.get(Textfield.class, "URItextField").setValue(DEFAULT_URI);
}

// Function to be activated when the "Run Server / Stop server" button is pressed
public void toggleServer() {
  String URI = cp5.get(Textfield.class, "URItextField").getText();
  serverRunning = !serverRunning;
  if (serverRunning) { 
    String gameStatus = "";
    int t = initClient(URI);
    println(t);
    if (t==-1) {
      gameStatus = "ERROR: cannot run server with given URI";
      serverRunning = false;  
      cp5.get(Textfield.class, "URItextField").setColor(RED);
    } else {
      gameStatus = "Server running. Waiting for players to ready up...";
      cp5.get(Button.class, "toggleServer").setCaptionLabel("Stop server");
      cp5.get(Textfield.class, "URItextField").setColor(GREEN);
    }
    cp5.get(Textlabel.class, "gameStatus")
      .setText(gameStatus);
  } else {
    client.disconnect();
    cp5.get(Textfield.class, "URItextField").setColor(WHITE);
    cp5.get(Textlabel.class, "gameStatus")
      .setText("Waiting for server to start running...");
    cp5.get(Button.class, "toggleServer").setCaptionLabel("Run server");
  }
}

public void setFont() {
  font = createFont("RussoOne-Regular.ttf", 20);
  textFont(font);
  textAlign(CENTER, CENTER);
}

public PApplet getApplet() {
  return this;
}

// Initialize mqtt client with specified URI
// Return -1 if error has occurred
// Otherwise, return 1.
public int initClient(String URI) {

  if (client == null) client = new MQTTClient(getApplet());

  try {
    client.connect(URI, CLIENT_ID);
    client.subscribe("#"); // Connect to all topics in this URI
  }
  catch(RuntimeException ex) {
    println("ERROR: failed to connect client using given information:\n|\tURI: "+URI+"\n|\tCLIENT_ID: "+CLIENT_ID);
    return -1;
  }
  return 1;
}

public void clientConnected() {
  ArrayList<Car> cars = game.getCars();
  if (cars.size()>=2) return;

  println("New client connected.");
  cars.add(new Car(cars.size()+1, ""));
}

// New mqtt message received with given topic and payload
public void messageReceived(String topic, byte[] payload) {
  String message = new String(payload);
  println("New message received: " + topic + " - " + message);
  String[] arr = topic.split("/");
  if (arr.length<2) return;
  if(arr[1].equals("readyState") && !game.gameRunning) readyState(arr[0], message);
  if(!game.gameRunning) return; // Don't run any "hit" or "rfid" commands if the game has not started yet
  
  switch(arr[1]) {
    case "rfid": 
      rfid(arr[0], message);
      break;
    case "hit": 
      hit(arr[0], message);
      break;
  }
}

public void connectionLost() {
  println("ERROR: Client connection lost");
  exit();
}



PVector uiBoxSize = new PVector(700,120);

class Car{
  private int health;
  private int id;
  private boolean ready;
  private String ip;
  private Optional<PowerUp> activePowerUp;
  private int animationTimer;
  
  Car(int id, String ip){
    this.health = 100;
    this.id = id;
    this.ready = false;
    this.ip = ip;
    this.animationTimer = 0;
    activePowerUp = Optional.empty();
  }
  
  public void restartCar(){
    this.health = 100;
    activePowerUp = Optional.empty();
  }
  
  public int getHealth(){
    return health;
  }
  
  public int getId(){
    return id;
  }
  
  public String getIP(){
    return ip;
  }
  
  public void hit(int damage){
    health-=damage;
    animationTimer = 30;
    if(health<=0){
      health = 0;
      game.gameOver();
    }
  }
  
  public void setPowerUp(PowerUp pow){
    activePowerUp = Optional.of(pow);
  }
  
  
  public void displayUI(){
    int leftX = round(uiBoxSize.x*0.03f);
    int rightX = round(uiBoxSize.x*0.65f);
    
    push();
    textAlign(CORNER);
    //Box
    strokeWeight(5);
    stroke(150);
    fill(GREY);
    rect(0,0,uiBoxSize.x,uiBoxSize.y,10);
    
    //Ready light
    fill(ready?GREEN:RED);
    ellipse(uiBoxSize.x*0.85f, uiBoxSize.y*0.6f, 40,40);
    
    //Texts
    fill(WHITE);
    textSize(40);
    text("PLAYER "+id, leftX, uiBoxSize.y/3);
    textSize(20);
    text("READY STATUS",uiBoxSize.x*0.7f, uiBoxSize.y/4);
    strokeWeight(10);
    stroke(lerpColor(RED,GREEN,health/100.0f));
    line(leftX, uiBoxSize.y*0.8f,map(health, 0,100, leftX, rightX), uiBoxSize.y*0.8f);

    if(animationTimer-- >0)fill(RED);
    else fill(WHITE);
    text("HEALTH: "+health,leftX, uiBoxSize.y*0.6f);
    pop();
  }
  
  public boolean getReadyStatus(){
    return ready;
  }
  
  // Setter for the ready status of the player
  // Check for game start after every set (if both players are ready)
  public void setReady(boolean status){
    ready=status;
    game.checkForGameStart();
  }
  
  public @Override
  String toString(){
    return "Car object:\n|\tPlayer: "+id +"\n|\tIP address:"+ip;
  }
}
public int RED,GREEN,GREY,BLACK,WHITE,GREY_STROKE,FONT_COLOR;

public void initColors(){
  colorMode(HSB,360,100,100);
  RED = color(0,100,100);
  GREEN = color(120,100,100);
  GREY = color(0,0,60);
  BLACK = color(0,0,0);
  WHITE = color(0,0,100);
  
  GREY_STROKE = color(0,0,50);
  FONT_COLOR = color(0,0,50);
}
// ======= Server -> Controller =======
public void broadcast(String topic, String message){
  for(Car c: game.getCars()){
    client.publish(c.getIP()+"/"+topic,message);
  }
}

public void health(Car car){
  client.publish(car.getIP()+"/health",String.valueOf(car.getHealth()));
}

public void getPowerup(Car car, PowerUp powUp){
  client.publish(car.getIP()+"/getPowerup",powUp.toString());
}

// ======= Controller -> Server =======
public void readyState(String carIP, String message){
  if(message.equals("0")) getCarByIp(carIP).setReady(false);
  else getCarByIp(carIP).setReady(true);
}

// ======= Car -> Server =======
public void rfid(String carIP, String message){
  Car c = getCarByIp(carIP);
  PowerUp p = PowerUp.getFromId(Integer.valueOf(message));
  c.setPowerUp(p); //Activate powerup for car with "carIP"
  getPowerup(c, p);
}

public void hit(String carIP, String message){
  Car c =getCarByIp(carIP);
  c.hit(10);
  health(c); //Send the updated health back to controller
}


// =======Helper functions =======

// Get opponent's Car object from IP
public Car getOpponentByIp(String carIP){
  for(Car c:game.getCars()){
    if(!c.getIP().equals(carIP)) return c;
  }
  System.out.println("ERROR: Unable to find opponent's car with IP "+carIP);
  return new Car(-1,"-1");
}

// Get Car object from IP
public Car getCarByIp(String carIP){
  for(Car c:game.getCars()){
    if(c.getIP().equals(carIP)) return c;
  }
  System.out.println("ERROR: Unable to find car with IP "+carIP);
  return new Car(-1,"-1");
}
class QuascarGame{
  ArrayList<Car> cars;
  boolean gameRunning = false;
  
  QuascarGame(){
    cars = new ArrayList();
    cars.add(new Car(cars.size()+1,"192.168.137.2"));
    cars.add(new Car(cars.size()+1,"192.168.137.3"));
  }
  
  public void display(){
    displayTitle();
    displayCars();
  }
  
  public void restartCars(){
    for(Car c: cars) c.restartCar();
  }

  public void startGame(){
    restartCars(); // Sets both cars to 100 health and no powerup
    gameRunning = true;
    cp5.get(Textlabel.class,"gameStatus").setText("Game started!");
    broadcast("gameState", "0"); // Send command to controllers that game has started
  }
  
  public ArrayList<Car> getCars(){
    return cars;
  }
  
  public void checkForGameStart(){
    for(Car c:cars){
      if(!c.getReadyStatus()) return;
    }
    startGame();
  }
  
  public void displayTitle(){
    push();
    textSize(50);
    text("QUASCAR", width/2, height*0.1f);
    textSize(30);
    text("CONNECTED CARS:", width*0.25f, height*0.2f);
    pop();
  }

  public void gameOver(){
    for(Car c: cars){
      c.setReady(false);
    }
    gameRunning = false;
    Car winner = cars.get(0).getHealth()<cars.get(1).getHealth()?cars.get(1):cars.get(0);
    String mess = "GAME OVER: Player "+winner.getId()+" won!\nBoth players need to ready up to start a new game.";
    println(mess);
    cp5.get(Textlabel.class,"gameStatus").setText(mess);
    broadcast("gameState", winner.getIP()); // Send command to controllers that game has ended. Announce winner
  }

  public void displayCars(){
    push();
    translate(50,height*0.25f);
    for(Car car:cars){
      car.displayUI();
      translate(0,uiBoxSize.y+MARGIN);
    }
    pop();
  }
}
enum PowerUp{
  SPEEDUP, LOCK_INPUT, SPIN, CONTINUOUS_LASER, SWITCH_DIRECTION, CANT_STOP, CANT_REVERSE, CANT_FORWARD;
  
  
  public static PowerUp getFromId(int powId){
    switch(powId){
      case 1: return SPEEDUP;
      case 2: return LOCK_INPUT;
      case 3: return SPIN;
      case 4: return CONTINUOUS_LASER;
      case 5: return SWITCH_DIRECTION;
      case 6: return CANT_STOP;
      case 7: return CANT_REVERSE;
      case 8: return CANT_FORWARD;
      default: return SPEEDUP;
    }
  }
  
  public String toString(){
    switch(this){
      case SPEEDUP: return "SPEEDUP";
      case LOCK_INPUT: return "LOCK_INPUT";
      case SPIN: return "SPIN";
      case CONTINUOUS_LASER: return "CONTINUOUS_LASER";
      case SWITCH_DIRECTION: return "SWITCH_DIRECTION";
      case CANT_STOP: return "CANT_STOP";
      case CANT_REVERSE: return "CANT_REVERSE";
      case CANT_FORWARD: return "CANT_FORWARD";
      default: return "SPEEDUP";
    }
  }
}
  public void settings() {  size(800, 800); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "processing_server" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
