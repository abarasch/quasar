// ======= Server -> Controller =======
void broadcast(String topic, String message){
  for(Car c: game.getCars()){
    client.publish(c.getIP()+"/"+topic,message);
  }
}

void health(Car car){
  client.publish(car.getIP()+"/health",String.valueOf(car.getHealth()));
}

void getPowerup(Car car, PowerUp powUp){
  client.publish(car.getIP()+"/getPowerup",powUp.toString());
}

// ======= Controller -> Server =======
void readyState(String carIP, String message){
  if(message.equals("0")) getCarByIp(carIP).setReady(false);
  else getCarByIp(carIP).setReady(true);
}

// ======= Car -> Server =======
void rfid(String carIP, String message){
  Car c = getCarByIp(carIP);
  PowerUp p = PowerUp.getFromId(Integer.valueOf(message));
  c.setPowerUp(p); //Activate powerup for car with "carIP"
  getPowerup(c, p);
}

void hit(String carIP, String message){
  Car c =getCarByIp(carIP);
  c.hit(10);
  health(c); //Send the updated health back to controller
}


// =======Helper functions =======

// Get opponent's Car object from IP
Car getOpponentByIp(String carIP){
  for(Car c:game.getCars()){
    if(!c.getIP().equals(carIP)) return c;
  }
  System.out.println("ERROR: Unable to find opponent's car with IP "+carIP);
  return new Car(-1,"-1");
}

// Get Car object from IP
Car getCarByIp(String carIP){
  for(Car c:game.getCars()){
    if(c.getIP().equals(carIP)) return c;
  }
  System.out.println("ERROR: Unable to find car with IP "+carIP);
  return new Car(-1,"-1");
}
