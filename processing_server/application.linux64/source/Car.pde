import java.util.Optional;

PVector uiBoxSize = new PVector(700,120);

class Car{
  private int health;
  private int id;
  private boolean ready;
  private String ip;
  private Optional<PowerUp> activePowerUp;
  private int animationTimer;
  
  Car(int id, String ip){
    this.health = 100;
    this.id = id;
    this.ready = false;
    this.ip = ip;
    this.animationTimer = 0;
    activePowerUp = Optional.empty();
  }
  
  void restartCar(){
    this.health = 100;
    activePowerUp = Optional.empty();
  }
  
  int getHealth(){
    return health;
  }
  
  int getId(){
    return id;
  }
  
  String getIP(){
    return ip;
  }
  
  void hit(int damage){
    health-=damage;
    animationTimer = 30;
    if(health<=0){
      health = 0;
      game.gameOver();
    }
  }
  
  void setPowerUp(PowerUp pow){
    activePowerUp = Optional.of(pow);
  }
  
  
  void displayUI(){
    int leftX = round(uiBoxSize.x*0.03);
    int rightX = round(uiBoxSize.x*0.65);
    
    push();
    textAlign(CORNER);
    //Box
    strokeWeight(5);
    stroke(150);
    fill(GREY);
    rect(0,0,uiBoxSize.x,uiBoxSize.y,10);
    
    //Ready light
    fill(ready?GREEN:RED);
    ellipse(uiBoxSize.x*0.85, uiBoxSize.y*0.6, 40,40);
    
    //Texts
    fill(WHITE);
    textSize(40);
    text("PLAYER "+id, leftX, uiBoxSize.y/3);
    textSize(20);
    text("READY STATUS",uiBoxSize.x*0.7, uiBoxSize.y/4);
    strokeWeight(10);
    stroke(lerpColor(RED,GREEN,health/100.0));
    line(leftX, uiBoxSize.y*0.8,map(health, 0,100, leftX, rightX), uiBoxSize.y*0.8);

    if(animationTimer-- >0)fill(RED);
    else fill(WHITE);
    text("HEALTH: "+health,leftX, uiBoxSize.y*0.6);
    pop();
  }
  
  boolean getReadyStatus(){
    return ready;
  }
  
  // Setter for the ready status of the player
  // Check for game start after every set (if both players are ready)
  void setReady(boolean status){
    ready=status;
    game.checkForGameStart();
  }
  
  @Override
  String toString(){
    return "Car object:\n|\tPlayer: "+id +"\n|\tIP address:"+ip;
  }
}
