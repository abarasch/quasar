#include <PubSubClient.h>

//mqtt
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

const uint16_t mqttPort = 1883;
const char server[] 
              //= "192.168.1.36";
              = "192.168.137.1";

void subscribeReceive(char* topic, byte* payload, unsigned int length)
{
  handle_powerup((char)payload[0] - '0');
}

void setupMqtt(){
   mqttClient.setServer(server, mqttPort);
   mqttClient.setCallback(subscribeReceive);
}

void reconnectMqtt(){
   while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect("quascarClient1")) {
      Serial.println("connected");
      // Subscribe
      mqttClient.subscribe(ipTopic);
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void mqttLoop(){
  if(!mqttClient.connected()){
    reconnectMqtt();
  }
  mqttClient.loop();
}

void mqttPublish(char* topic, char* message){
  mqttClient.publish(topic, message);
}
