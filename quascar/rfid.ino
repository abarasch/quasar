#include <SoftwareSerial.h>

//RFID
SoftwareSerial ssrfid(D6,8); 

const int BUFFER_SIZE = 14; // Size of datagram

uint8_t buffer[BUFFER_SIZE]; // used to store an incoming data frame 
uint8_t tag[10][BUFFER_SIZE] = 
{{2,54,53,48,48,53,65,68,52,54,70,56,52,3},
{2,54,53,48,48,53,56,57,67,50,65,56,66,3},
{2,54,52,48,48,65,69,56,56,53,52,49,54,3},
{2,54,52,48,48,65,69,69,67,54,50,52,52,3},
{2,54,53,48,48,53,56,55,53,49,54,53,69,3},
{2,54,67,48,48,65,48,65,50,69,53,56,66,3},
{2,54,53,48,48,53,56,54,55,66,65,69,48,3},
{2,54,52,48,48,65,69,56,49,69,55,65,67,3},
{2,54,53,48,48,53,56,53,51,55,67,49,50,3},
{2,54,53,48,48,53,65,68,65,55,69,57,66,3}};

int buffer_index = 0;

void startRFID(){
 ssrfid.begin(9600);
 ssrfid.listen();   
 Serial.println("RFID init.");
}

uint8_t listenRFID(){
  if(ssrfid.available() <= 0){ // RFID not available
    return 0;
  }
  bool call_extract_tag = false;
  
  int ssvalue = ssrfid.read(); // read 
  if (ssvalue == -1) { // no data was read
    return 0;
  }
  if (ssvalue == 2) { // RDM630/RDM6300 found a tag => tag incoming 
    buffer_index = 0;
  } else if (ssvalue == 3) { // tag has been fully transmitted       
    call_extract_tag = true; // extract tag at the end of the function call
  }
  if (buffer_index >= BUFFER_SIZE) { // checking for a buffer overflow (It's very unlikely that an buffer overflow comes up!)
    Serial.println("Error: Buffer overflow detected!");
    return -1;
  }
  
  buffer[buffer_index++] = ssvalue; // everything is alright => copy current value to buffer
  if (call_extract_tag == true) {
    if (buffer_index == BUFFER_SIZE) {
      bool check;
      for(uint8_t tagNr=0;tagNr<10;tagNr++){
        check=true;
        for(buffer_index=BUFFER_SIZE-1;buffer_index>0;buffer_index--){
          if(buffer[buffer_index]!=tag[tagNr][buffer_index]){check=false;}
        }
        if(check){
          Serial.print("Tag number ");
          Serial.println(tagNr+1);
          return tagNr+1;
        }
      }
    } else { // something is wrong... start again looking for preamble (value: 2)
      buffer_index = 0;
      return -1;
    }
  }

  // TODO, fix not all paths return a value, check what should be returned here, is 0 a good value?
  return 0;
}
