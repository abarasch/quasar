/* 
 *  Used pins:
 *  
 *  D0  16    Light sensor (BACK)
 *  D1  5     I2c
 *  D2  4     I2c
 *  D3  0     (oled_reset ?) Light Sensor (SIDE1)
 *  D4  2     builtin led, Laser
 *  D5  14    CLK
 *  D6  12    RFID
 *  D7  13    (old: LED-matrix) Light Sensor (SIDE2) 
 *  D8  15    Servo
 *  RX  3
 *  TX  1
 */

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <Adafruit_GFX.h>
#include <WEMOS_Matrix_GFX.h>
#include <Adafruit_SSD1306.h>

#define NONE 0
#define FORWARD 1
#define BACKWARD 2
#define RIGHT 4
#define LEFT 8
#define LASER_R 16
#define LASER_L 32
#define SHOOT 64
#define ISPOWERUP 128



Adafruit_SSD1306 display(D3);

//SSID of your network
const char wifi_ssid[] 
            //= "H220N141535"; // Lab's router SSID
            = "QUASCAR";    // Mobile hotspot SSID
            
const char wifi_pass[] 
            //= "W817%gigaset%sx551"; //Lab's rotuer password
            = "0V31{7g8"; // Mobile hotspot password

// Car's static ip address (CHANGE ALL)
IPAddress ip(192, 168, 137, 2); 
char ipTopic[] = "192.168.137.2/doPowerup";
char ipRFID[] = "192.168.137.2/rfid";
char ipHit[] = "192.168.137.3/hit";

IPAddress gateway(192, 168, 137, 1);
IPAddress DNS(192, 168, 137, 1);
IPAddress subnet(255, 255, 255, 0);  //Subnet mask
            
bool wifi_connected = false;
const int BACK = D0;
const int SIDE1 = D3;
const int SIDE2 = D7;

int previousBack = LOW;
int previousSide1 = LOW;
int previousSide2 = LOW;
unsigned long lastDebounceTime_b = 0;
unsigned long lastDebounceTime_s1 = 0;
unsigned long lastDebounceTime_s2 = 0;
unsigned long debounceDelay = 50;

//server connection
const uint16_t port = 4210;

WiFiUDP Udp;
char incomingPacket[255];  // buffer for incoming packets

void startListening(){
  Udp.begin(port);  
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), port);
  display.clearDisplay();
  display.setCursor(0,10);
  display.printf("%s:%d", WiFi.localIP().toString().c_str(), port);
  display.display();
}

void connectWifi(){
  Serial.printf("Connecting to %s ", wifi_ssid);
  WiFi.config(ip, gateway, subnet, DNS);
  delay(100);
  WiFi.begin(wifi_ssid, wifi_pass);
  Serial.print("Connecting");
 
  display.clearDisplay();
  display.printf("Connecting...");
  display.display();

  while (WiFi.status() != WL_CONNECTED) {
      Serial.print(".");
      delay(200);
  }
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println();
    Serial.println("Fail connecting");
    delay(5000);
    ESP.restart();
  }
  Serial.print("   OK  ");
  Serial.print("Module IP: ");
  Serial.println(WiFi.localIP());
  
  wifi_connected = true;
  Serial.println(" connected");
}

void startScreen(){
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 64x48)
  drawLogo(2000);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 10);
}

void setup() {      
  pinMode(D4, OUTPUT);
  pinMode(BACK, INPUT);
  pinMode(SIDE1, INPUT);
  pinMode(SIDE2, INPUT);
  
  Serial.begin(9600);
  Serial.println();
  
  setup_motors(60);
  setup_servo();
  startScreen();  
  connectWifi();  
  startListening();
  setupMqtt();
  startRFID();
}

uint8_t rfidReading;
char messageStr[1024];

void loop() {
  mqttLoop();
  char command;
  
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    // receive incoming UDP packets
    //Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(incomingPacket, 255);
    if(len == 1){
      //Serial.printf("UDP packet contents: %d\n", incomingPacket[0]);    
      command = incomingPacket[0];
    }
    handle_input(command);
  }

  // reading the RFID sensors
  rfidReading = listenRFID();
  if(rfidReading == -1){
    Serial.println("RFID ERROR"); 
  }else if(rfidReading != 0){
    char msg[1];
    msg[0] = char('1' + rfidReading);
    mqttPublish(ipRFID, msg);
  }
  // reading the light sensors
  int currentBack = digitalRead(BACK);
  int currentSide1 = digitalRead(SIDE1);
  int currentSide2 = digitalRead(SIDE2);  

  if (currentSide1 == HIGH) {
      Serial.println("hit on side 1");
      mqttPublish(ipHit, "2");
  }
  previousSide1 = currentSide1;

  if (currentSide2 == HIGH) {
     Serial.println("hit on side 2");
     mqttPublish(ipHit, "2");
  }
  previousSide2 = currentSide2;

  if (currentBack == HIGH) {
     Serial.println("hit on back");
     mqttPublish(ipHit, "2");
  }
  previousBack = currentBack;
}
