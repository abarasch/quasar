#define SPEEDUP 1
#define LOCK_INPUT 2
#define SPIN 3
#define CONTINUOUS_LASER 4
#define SWITCH_DIRECTIONS 5
#define CANT_STOP 6
#define CANT_REVERSE 7
#define CANT_FORWARD 8

char rememberedCommand;

void startPowerup(char powerup, char lastcommand) {
  switch(powerup) {
    case SPEEDUP: set_speed(MAX_SPEED); break;
    case LOCK_INPUT: rememberedCommand = lastcommand; break;
  }
  return;
}

char doPowerup(char powerup, char command, char lastcommand) {
  printPowerup(powerup);
  switch(powerup) {
    case LOCK_INPUT:          // sets input to the remembered command set at start of powerup
      return rememberedCommand;
    case SPIN:                // turns directional input into only right, but keeps the rest
      return (command & (LASER_L | LASER_R | SHOOT)) | RIGHT;
    case CONTINUOUS_LASER:    // shoot is enabled rest of controls is kept.
      return command | SHOOT;
    case SWITCH_DIRECTIONS:   // switches the inputs left and right
      return (command & ~(LEFT | RIGHT)) | (command & RIGHT)<<1 | (command & LEFT)>>1; 
    case CANT_STOP:           // returns the previous command if the current command is nothing
      return command | (!command * lastcommand);
    case CANT_REVERSE:        // always sets the backward input to 0
      return command & ~BACKWARD;
    case CANT_FORWARD:        // allways sets the forward input to 0
      return command & ~FORWARD;
    default:
      return command;
  }
}

void printCommand(char command) {
  Serial.printf("%d: \n", command);
  if(command & FORWARD){
    Serial.printf("FORWARD\n");
  }
  if(command & BACKWARD){
    Serial.printf("BACKWARD\n");
  }
  if(command & RIGHT){
    Serial.printf("RIGHT\n");
  }
  if(command & LEFT){
    Serial.printf("LEFT\n");    
  }
  if(command & LASER_R){
    Serial.printf("LASER_R\n");
  }
  if(command & LASER_L){
    Serial.printf("LASER_L\n");
  }
  if(command & SHOOT){
    Serial.printf("SHOOT\n");
  }
  return;
}

void printPowerup(char powerup) {
  Serial.printf("Powerup: ");
  switch(powerup) {
    case LOCK_INPUT:          // sets input to the remembered command set at start of powerup
      Serial.printf("LOCK_INPUT\n"); return;
    case SPIN:                // turns directional input into only right, but keeps the rest
      Serial.printf("SPIN\n"); return;
    case CONTINUOUS_LASER:    // shoot is enabled rest of controls is kept.
      Serial.printf("CONTINUOUS_LASER\n"); return;
    case SWITCH_DIRECTIONS:   // switches the inputs left and right
      Serial.printf("SWITCH_DIRECTIONS\n"); return;
    case CANT_STOP:           // returns the previous command if the current command is nothing
      Serial.printf("CANT_STOP\n"); return;
    case CANT_REVERSE:        // always sets the backward input to 0
      Serial.printf("CANT_REVERSE\n"); return;
    case CANT_FORWARD:        // allways sets the forward input to 0
      Serial.printf("CANT_FORWARD\n"); return;
    case SPEEDUP:
      Serial.printf("SPEEDUP"); return;
    default:
      Serial.printf("ERROR:INVALID POWERUP CODE"); return;
  }  
}

void endPowerup(char powerup) {
  switch(powerup) {
    case SPEEDUP: set_speed(DEFAULT_SPEED); break;
  }
  return;
}
