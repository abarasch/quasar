#include <Servo.h>

Servo myservo;  // create servo object to control the laser

// upper and lower limits of angle
const int lowest_angle = 10;
const int highest_angle = 170;

int angle = 90; // variable to store the laser position

void setup_servo() {
  myservo.attach(D8);
}

void turn_laser_left() {
  if(angle < highest_angle) {
    angle += 5;
  }
  turn_laser();
}

void turn_laser_right() {
  if(angle > lowest_angle) {
    angle -= 5;
  }
  turn_laser();
}

void turn_laser() {
  Serial.println(angle);
  Serial.println(myservo.attached());
  myservo.write(angle);
}
