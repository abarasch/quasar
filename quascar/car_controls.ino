#include <LOLIN_I2C_MOTOR.h>

//Constants for speed 
#define MAX_SPEED 100
#define DEFAULT_SPEED 60

//Duty fraction for turns
#define FULLTURN 0.7
#define HALFTURN 0.5

LOLIN_I2C_MOTOR motors;

unsigned char spd[3];

/* Directions of car movement. Use a bitwise OR (|) for multiple directions at the same time.
#define NONE 0

#define FORWARD 1
#define BACKWARD 2

#define RIGHT 4
#define LEFT 8

#define FORWARD_RIGHT FORWARD|RIGHT // = 5
#define FORWARD_LEFT FORWARD|LEFT // = 9

#define BACKWARD_RIGHT BACKWARD|RIGHT // = 6
#define BACKWARD_LEFT BACKWARD|LEFT // = 10
*/

enum duties{full, turn, half_turn};

// direction and duty used for a single motor(shield)
struct dir_and_duty{
  unsigned char dir;
  duties duty;
  
  bool operator== (dir_and_duty r) {
    return (*this).dir == r.dir && (*this).duty == r.duty; 
  }
};

// setting (direction and duty) for both sides
struct setting_type{
  dir_and_duty left;
  dir_and_duty right;
  
  bool operator== (setting_type r) {
    return (*this).left == r.left && (*this).right == r.right; 
  }
};

// used to get a setting for all the motors based on a direction
const setting_type NO_ENCODING = {{0,full},{0,full}};
const setting_type direction_settings[11] = {
                    NO_ENCODING,
                    {{MOTOR_STATUS_CW,full}, {MOTOR_STATUS_CW,full}}, // FORWARD
                    {{MOTOR_STATUS_CCW,full}, {MOTOR_STATUS_CCW,full}}, //BACKWARD
                    NO_ENCODING,
                    {{MOTOR_STATUS_CW,turn}, {MOTOR_STATUS_CCW,turn}}, //RIGHT 
                    {{MOTOR_STATUS_CW,full}, {MOTOR_STATUS_CW,half_turn}}, //FORWARD RIGHT
                    {{MOTOR_STATUS_CCW,full}, MOTOR_STATUS_CCW,half_turn}, //BACKWARD RIGHT
                    NO_ENCODING,
                    {{MOTOR_STATUS_CCW,turn}, {MOTOR_STATUS_CW,turn}}, // LEFT
                    {{MOTOR_STATUS_CW,half_turn}, {MOTOR_STATUS_CW,full}}, // FORWARD LEFT
                    {{MOTOR_STATUS_CCW,half_turn}, {MOTOR_STATUS_CCW,full}}}; // BACKWARD LEFT

// run this function to be able to use the motors. if field is left empty DEFAULT SPEED is used.
void setup_motors() {
  motors.changeFreq(MOTOR_CH_BOTH, 1000);
  set_speed(DEFAULT_SPEED);
}
void setup_motors(float speed) {
  motors.changeFreq(MOTOR_CH_BOTH, 1000);
  set_speed(speed);
}

// The car will move in direction dir. (see encoding)
void go(int dir) {
  if(0 <= dir && dir < 11) {
    setting_type setting = direction_settings[dir];
    /*if(setting == NO_ENCODING) {
      brake();
      return;
    }*/
    // set the status and duty
    motors.changeStatus(MOTOR_CH_A, setting.left.dir);
    motors.changeStatus(MOTOR_CH_B, setting.right.dir);
    motors.changeDuty(MOTOR_CH_A, spd[setting.left.duty]);
    motors.changeDuty(MOTOR_CH_B, spd[setting.right.duty]);
  } else {
    brake();
  }
}

// call to quickly stop the car/motors. 
void brake() {
  motors.changeStatus(MOTOR_CH_BOTH, MOTOR_STATUS_SHORT_BRAKE);
}

// set the duty of the motors. between 0 and 100. Turn duties are calculated as a fraction of this speed.
void set_speed(unsigned char speed) { 
  if(speed > 0 && speed <= MAX_SPEED) {
    spd[0] = speed;
    spd[1] = speed*FULLTURN;
    spd[2] = speed*HALFTURN;
  }
}
