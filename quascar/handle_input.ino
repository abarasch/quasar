#define NONE 0
#define FORWARD 1
#define BACKWARD 2
#define RIGHT 4
#define LEFT 8
#define LASER_R 16
#define LASER_L 32
#define SHOOT 64

#define seconds *40 // change this to accurately get a second

char lastcommand = 0;
char command = 0;
int poweredUp = 0;
char powerup;

int durations[] = {0, 
      /*speedup:*/ 8 seconds, 
      /*lock input:*/ 2 seconds, 
      /*spin:*/ 1 seconds, 
      /*continuous laser:*/ 4 seconds, 
      /*switch directions:*/ 4 seconds, 
      /*cant stop:*/ 4 seconds, 
      /*cant reverse:*/ 4 seconds, 
      /*cant forward:*/ 4 seconds};

//led matrix
MLED matrix(7); //set intensity=7 (maximum)

void updateState(){
  matrix.clear();

  printCommand(command);

  if(command & LASER_R){
    turn_laser_right();
  }
  if(command & LASER_L){
    turn_laser_left();     
  }
  if(command & SHOOT){
    digitalWrite(D4, HIGH);
  }else{
    digitalWrite(D4, LOW);
  }
  
  if(command && lastcommand != command) {
    go(command);
  }
  if(command == 0){
    brake();
  }

  lastcommand = command;
  matrix.writeDisplay();
}

void handle_powerup(char p) {
  // PoweredUp is a counter that is larger than 0 if there is a powerup active. The powerup that is active is encoded by powerup, which is the same character as the powerup command.
  // If we recieve a new powerup the current one is overwritten. 
  if(poweredUp) {
      endPowerup(powerup);
  }
  powerup = p;
  poweredUp = durations[powerup];
  startPowerup(command, lastcommand);
}

void handle_input(char c) {
  //Serial.printf("Before command processing:\n");
  //printCommand(command);
  if(poweredUp) {
    command = doPowerup(powerup, c, lastcommand);
    //Serial.printf("After processing:\n");
    //printCommand(command);
    
    // if the powerUp is not longer active after subtracting 1, do possible ending function for that powerup
    if(!--poweredUp) {
      endPowerup(powerup);
    }
  } else {
    command = c;
  }
  //Updatestate updates the matrix and executes command
  updateState();
}
