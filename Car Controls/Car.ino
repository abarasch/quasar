/*
 * Koen Sauren | s1024202
 * Cas Haaijman | s4372662
 * 
*/
#include <LOLIN_I2C_MOTOR.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "paj7620.h"

#define GES_REACTION_TIME 500
#define GES_ENTRY_TIME 800
#define GES_QUIT_TIME 1000
#define I2C_ADDRESS 0x43
#define I2C_ADDRESS2 0x44
#define OLED_RESET 0

//Constants for speed, 
#define MAX_SPEED 100
#define FORWARD 0
#define RIGHT 1
#define BACKWARD 2
#define LEFT 3

//Motor
LOLIN_I2C_MOTOR left_motors;
LOLIN_I2C_MOTOR right_motors(111);

void setup()
{
} 

void loop()
{
  //test of all functions
  
  for(int dir = 0; dir < 4; dir++) {
    go(dir);
    powerMotors(20);
    delay(1000);
    brake();
  }
  
}

void go(int dir) {
  unsigned char directions[4][2] = {{MOTOR_STATUS_CW, MOTOR_STATUS_CW}, {MOTOR_STATUS_CW, MOTOR_STATUS_CCW}, {MOTOR_STATUS_CCW, MOTOR_STATUS_CW}, {MOTOR_STATUS_CCW, MOTOR_STATUS_CCW}};
  left_motors.changeStatus(MOTOR_CH_BOTH, directions[dir][0]);
  right_motors.changeStatus(MOTOR_CH_BOTH, directions[dir][1]);
}

void brake() {
  left_motors.changeStatus(MOTOR_CH_BOTH, MOTOR_STATUS_SHORT_BRAKE);
  right_motors.changeStatus(MOTOR_CH_BOTH, MOTOR_STATUS_SHORT_BRAKE);
}

void powerMotors(float speed) {
  if(speed > 0 && speed <= MAX_SPEED) {
    left_motors.changeFreq(MOTOR_CH_BOTH, 1000);
    right_motors.changeFreq(MOTOR_CH_BOTH, 1000);
    left_motors.changeDuty(MOTOR_CH_BOTH, speed);
    right_motors.changeDuty(MOTOR_CH_BOTH, speed);
    //motor.changeStatus(MOTOR_CH_A, MOTOR_STATUS_STOP);
  }
}
